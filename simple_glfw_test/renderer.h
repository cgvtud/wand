#ifndef RENDERER_H
#define RENDERER_H
#include "viewSettings.h"
#include "box.h"
#include "projectionUtil.h"

class renderer {
public:
  renderer();
  ~renderer();
  
  void doRender(const projectionUtil& proj, const viewSettings& vs);
  
};

#endif // RENDERER_H
