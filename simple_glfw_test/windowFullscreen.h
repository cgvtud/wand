#ifndef WINDOWFULLSCREEN_H
#define WINDOWFULLSCREEN_H
#include <GLFW/glfw3.h>

class windowFullscreen {
public:
  static bool init();
  static void deinit();
  
  windowFullscreen();
  ~windowFullscreen();
  
  void close();
  inline bool isValid() const {
    return wnd != nullptr;
  }
  inline int getWidth() const { return width; }
  inline int getHeight() const { return height; }
  
  void doEvents();
  void beginRender();
  void endRender();
  
private:
  GLFWwindow *wnd;
  int width, height;
 
};

#endif // WINDOWFULLSCREEN_H
