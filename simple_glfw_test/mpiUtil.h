#ifndef MPIUTIL_H
#define MPIUTIL_H
#include <mpi.h>

///
/// Simple utility class managing some aspects of MPI
class mpiUtil {
public:
  
  /// Singleton instance
  static mpiUtil& inst();
  static inline int rank() { return inst().mpiRank; }
  inline int myRank() { return mpiRank; }
  
  bool init(int * argc, char *** argv);
  void deinit();
  
private:
  mpiUtil();
  ~mpiUtil();
  
  int mpiRank;
  
};

#endif // MPIUTIL_H
