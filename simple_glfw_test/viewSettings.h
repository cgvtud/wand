#ifndef VIEWSETTINGS_H
#define VIEWSETTINGS_H
#pragma once

typedef struct _viewsettings_t {
  bool isAlive;
  float boundingBox[6]; // xyzxyz - internally identical to box class
  float viewMatrix[16];
  float projFovyRad; // field of view (Y) in radians
  float nearClip;
  float farClip;
} viewSettings;

#endif /* VIEWSETTINGS_H */
