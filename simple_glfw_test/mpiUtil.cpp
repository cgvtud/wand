#include "mpiUtil.h"
#include <cassert>
#include <iostream>

mpiUtil& mpiUtil::inst() {
  static mpiUtil i;
  return i;
}

bool mpiUtil::init(int * argc, char *** argv) {
  if (::MPI_Init(argc, argv) != MPI_SUCCESS) {
    std::cerr << "MPI_Init failed" << std::endl;
    return false;
  }
    
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
  
  return true;
}

void mpiUtil::deinit() {
  if (mpiRank == -1) return;
  MPI_Finalize();   
  mpiRank = -1;
}

mpiUtil::mpiUtil() : mpiRank(-1) {
  // intentionally empty
}

mpiUtil::~mpiUtil() {
  assert(mpiRank == -1);
}
