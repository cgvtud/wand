#include "windowFullscreen.h"
#include <iostream>
#include "mpiUtil.h"
#include <GL/gl.h>
#include <thread>

bool windowFullscreen::init() {
  if (::glfwInit() != GL_TRUE) {
    std::cerr << mpiUtil::rank() << " glfwInit failed" << std::endl;
    return false;
  }
  std::cout << mpiUtil::rank() << " glfwInit" << std::endl;
  return true;
}

void windowFullscreen::deinit() {
  ::glfwTerminate();
  std::cout << mpiUtil::rank() << " glfwTerminate" << std::endl;
}

windowFullscreen::windowFullscreen() : wnd(nullptr), width(-1), height(-1) {
  
  //
  // the next GLFW window will request the following OpenGL context settings:  
  //
  ::glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // ogl 3.3 core
  ::glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  // ::glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); not using this line allows for old stuff
  ::glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

  //
  // Collect the size of the desktop by combining of the size of all monitors
  //
  int monsX1, monsY1, monsX2, monsY2;
  int monsCount;
  GLFWmonitor** monitors = ::glfwGetMonitors(&monsCount);
//  std::cout << mpiUtil::rank() << ": " << monsCount << " monitors:" << "\n";
  for (int i = 0; i < monsCount; ++i) {
    const GLFWvidmode* mode = ::glfwGetVideoMode(monitors[i]);
    int xpos, ypos;
    ::glfwGetMonitorPos(monitors[i], &xpos, &ypos);
    
    if (i == 0) {
      monsX1 = xpos;
      monsY1 = ypos;
      monsX2 = xpos + mode->width;
      monsY2 = ypos + mode->height;
    } else {
      if (monsX1 > xpos) monsX1 = xpos;
      if (monsY1 > ypos) monsY1 = ypos;
      if (monsX2 < xpos + mode->width) monsX2 = xpos + mode->width;
      if (monsY2 < ypos + mode->height) monsY2 = ypos + mode->height;
    }
    
//    std::cout << mpiUtil::rank() << "\t" << mode->width << " x " << mode->height << "\t at " << xpos << ", " << ypos << " with " << mode->refreshRate << "Hz \n";
  }
//  std::cout << std::endl;

  //
  // create a top-most window without decorations
  // first make a smaller window, because the create command cannot specify the initial position
  //
  ::glfwWindowHint(GLFW_DECORATED, GL_FALSE);
  ::glfwWindowHint(GLFW_FLOATING, GL_TRUE);
  ::glfwWindowHint(GLFW_AUTO_ICONIFY, GL_FALSE);
  wnd = ::glfwCreateWindow(200, 200, "window", nullptr, nullptr);
  ::glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    
  if (!wnd) {
    std::cerr << mpiUtil::rank() << " glfwCreateWindow failed" << std::endl;
    wnd = nullptr;
  } else {
    std::cout << mpiUtil::rank() << " glfwCreateWindow" << std::endl;
  }
    
  //
  // move and resize the window to cover the whole screen
  //
  ::glfwSetWindowPos(wnd, monsX1, monsY1);
  ::glfwSetWindowSize(wnd, monsX2 - monsX1, monsY2 - monsY1);
  std::cout << mpiUtil::rank() << " Window (" << monsX1 << ", " << monsY1 << ") x (" << monsX2 - monsX1 << ", " << monsY2 - monsY1 << ")" << std::endl;
  
  //
  // blank the window content to black for now
  //
  for (int i = 0; i < 10; ++i) {
    // this is all ugly, but seems necessary due to the asynchronous nature of the xserver
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    ::glfwPollEvents();
    
    ::glfwMakeContextCurrent(wnd);
    ::glViewport(0, 0, monsX2 - monsX1, monsY2 - monsY1);
    ::glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ::glfwSwapBuffers(wnd);
  }
  
}

windowFullscreen::~windowFullscreen() {
  close();
}

void windowFullscreen::close() {
  if (wnd != nullptr) {
    ::glfwDestroyWindow(wnd);
    wnd = nullptr;
  }
}

void windowFullscreen::doEvents() {
  ::glfwPollEvents();
  if (wnd == nullptr) return;
  
  int w, h;
  ::glfwGetFramebufferSize(wnd, &w, &h);
  if ((width != w) || (height != h)) {
    width = w;
    height = h;
    ::glfwMakeContextCurrent(wnd);
    glViewport(0, 0, w, h);
  }
  
  if (::glfwWindowShouldClose(wnd)) {
    close();
  }
}

void windowFullscreen::beginRender() {
  if (wnd == nullptr) return;
  ::glfwMakeContextCurrent(wnd);
  ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void windowFullscreen::endRender() {
  if (wnd == nullptr) return;
  ::glfwSwapBuffers(wnd);
}
