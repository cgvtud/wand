#ifndef PROJECTIONUTIL_H
#define PROJECTIONUTIL_H
#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "viewSettings.h"

class projectionUtil {
public:
  projectionUtil();
  ~projectionUtil();
  
  inline void setupTile(int fullWidth, int fullHeight, int tileX, int tileY, int tileWidth, int tileHeight) {
    this->fullWidth = fullWidth;
    this->fullHeight = fullHeight;
    this->tileX = tileX;
    this->tileY = tileY;
    this->tileWidth = tileWidth;
    this->tileHeight = tileHeight;
    this->updateProjMat();
  }
  
  void set(float fovyRad, float nearClip, float farClip);
  inline void set(const viewSettings& vs) {
    set(vs.projFovyRad, vs.nearClip, vs.farClip);
  }
  
  void setAspect(float ar);
  inline void setViewSize(int width, int height) { setAspect(static_cast<float>(width) / static_cast<float>(height)); }
    
  inline const float* projection() const {
    return glm::value_ptr(proj);
  }
  
private:
  void updateProjMat();
  
  glm::mat4 proj;
  float fovyRad, nearClip, farClip;
  float aspectRatio;
  
  int fullWidth, fullHeight;
  int tileX, tileY, tileWidth, tileHeight;
  
};

#endif // PROJECTIONUTIL_H
