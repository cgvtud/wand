#include "renderer.h"
#include <GL/gl.h>

renderer::renderer()/* : bbox(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f)*/ {
}

renderer::~renderer() {
  // intentionally empty
}

void renderer::doRender(const projectionUtil& proj, const viewSettings& vs) {
  box bbox(vs.boundingBox[0], vs.boundingBox[1], vs.boundingBox[2], vs.boundingBox[3], vs.boundingBox[4], vs.boundingBox[5]);

  ::glMatrixMode(GL_PROJECTION);
  ::glLoadMatrixf(proj.projection());
  
  ::glMatrixMode(GL_MODELVIEW);
  ::glLoadMatrixf(vs.viewMatrix);
  
  ::glBegin(GL_LINES);

  ::glColor3ub(192, 192, 192);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z2);

  ::glVertex3f(bbox.x1, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z2);

  ::glVertex3f(bbox.x1, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z2);
  
  ::glColor3ub(255, 0, 0);
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zc());
  ::glVertex3f(bbox.xma(), bbox.yc(), bbox.zc());
  
  ::glColor3ub(0, 255, 0);
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zc());
  ::glVertex3f(bbox.xc(), bbox.yma(), bbox.zc());
  
  ::glColor3ub(32, 64, 255);
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zc());
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zma());
  
  ::glEnd();
  
}

