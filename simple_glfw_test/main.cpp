#include <iostream>
#include <thread>
#include "mpiUtil.h"
#include "windowFullscreen.h"
#include <zmq.hpp>
#include "renderer.h"
#include "viewSettings.h"
#include "projectionUtil.h"
#include <glm/gtc/matrix_transform.hpp>
#include "zmqclient.h"


///
/// Application main entry point
///
int main(int argc, char **argv) {
  //
  // initialize libraries
  //
  if (!mpiUtil::inst().init(&argc, &argv)) return -1;
  if (!windowFullscreen::init()) {
    mpiUtil::inst().deinit();
    return -2;
  }
  
  //
  // create fullscreen window for opengl rendering
  //
  windowFullscreen window;
  
  //
  // initialize application
  //
  viewSettings view;
  projectionUtil proj;
  renderer rnd;
  zmqclient *client = nullptr;

  view.isAlive = true;
  proj.setViewSize(window.getWidth(), window.getHeight());
  proj.setupTile( // TODO: this should be a config file
    5760, 2160, // full wall size
    1920 * mpiUtil::rank(), 0,
    1920, 2160  
  );

  if (mpiUtil::rank() == 0) {
    //
    // first rank 0 establishes connection with the head node on 'mary'
    //
    client = new zmqclient("mary", 23452);
    std::cout << mpiUtil::rank() << " ZMQ Connecting to head node ..." << std::endl;
    if (!client->initPing()) {
      std::cerr << mpiUtil::rank() << " ZMQ init ping handshake failed" << std::endl;
      view.isAlive = false;
    }


    //
    // TODO: implement
    // here we initialize the bounding box
    //
    view.boundingBox[0] = -3.0f;
    view.boundingBox[1] = view.boundingBox[2] = -1.0f;
    view.boundingBox[3] = 3.0f;
    view.boundingBox[4] = view.boundingBox[5] = 1.0f;
    

    if (view.isAlive) {
      client->sendBoundingBox(view.boundingBox);
      if (!client->updateViewSettings(view)) {
	view.isAlive = false;
      }
    }
   
  }
  
  // Add initialization tasks common to all MPI nodes here
  
  
  // wait for MPI world to finish initialization
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Bcast(&view, sizeof(viewSettings), MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);
  
  //
  // application main event loop
  //
  while(view.isAlive) {
    window.doEvents();
    
    if (window.isValid()) {
      
      window.beginRender();

      // performing the actual rendering    
      rnd.doRender(proj, view);
      
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      
      // synchronizing the final buffer swaps as good as possible.
      ::glFlush();
      MPI_Barrier(MPI_COMM_WORLD);
      
      window.endRender();
    
    }
    
    if (view.isAlive && (mpiUtil::rank() == 0)) {
      // udpate viewSettings by communicating with the host
      if (client != nullptr) {
	if (!client->updateViewSettings(view)) {
	  view.isAlive = false;
	}
      }
    }
    
    // synchronizing view settings in the whole MPI world
    MPI_Bcast(&view, sizeof(viewSettings), MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);
  }
  
  
  //
  // cleanup environment
  //
  if (client != nullptr) {
    delete client;
    client = nullptr;
  }
  window.close();
  windowFullscreen::deinit();
  mpiUtil::inst().deinit();
  return 0;
}
