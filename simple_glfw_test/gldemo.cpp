/*
#include <iostream>
#include <thread>
#include "mpi.h"
#include "GLFW/glfw3.h"

int main(int argc, char **argv) {
    if (::MPI_Init(&argc, &argv) != MPI_SUCCESS) {
	std::cerr << "MPI_Init failed" << std::endl;
	return -1;
    }
    
    if (::glfwInit() != GL_TRUE) {
        std::cerr << "glfwInit failed" << std::endl;
        return -1;
    }
    
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int monsX1, monsY1, monsX2, monsY2;
    int monsCount;
    GLFWmonitor** monitors = glfwGetMonitors(&monsCount);
    
    std::cout << rank << ": " << monsCount << " monitors:" << "\n";
    for (int i = 0; i < monsCount; ++i) {
	const GLFWvidmode* mode = glfwGetVideoMode(monitors[i]);
	int xpos, ypos;
	glfwGetMonitorPos(monitors[i], &xpos, &ypos);
	
	if (i == 0) {
	    monsX1 = xpos;
	    monsY1 = ypos;
	    monsX2 = xpos + mode->width;
	    monsY2 = ypos + mode->height;
	} else {
	    if (monsX1 > xpos) monsX1 = xpos;
	    if (monsY1 > ypos) monsY1 = ypos;
	    if (monsX2 < xpos + mode->width) monsX2 = xpos + mode->width;
	    if (monsY2 < ypos + mode->height) monsY2 = ypos + mode->height;
	}
	
	std::cout << rank << "\t" << mode->width << " x " << mode->height << "\t at " << xpos << ", " << ypos << " with " << mode->refreshRate << "Hz \n";
    }
    std::cout << std::endl;
    
    ::glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // ogl 3.3 core
    ::glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    // ::glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); allow old stuff
    ::glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
    
    
    GLFWwindow *wnd;
    ::glfwWindowHint(GLFW_DECORATED, GL_FALSE);
    ::glfwWindowHint(GLFW_FLOATING, GL_TRUE);
    ::glfwWindowHint(GLFW_AUTO_ICONIFY, GL_FALSE);

    wnd = ::glfwCreateWindow(200, 200, "window", nullptr, nullptr);
    glfwSetInputMode(wnd, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    
    if (!wnd) {
	std::cerr << "glfwCreateWindow failed" << std::endl;
	wnd = nullptr;
    }
    
    glfwSetWindowPos(wnd, monsX1, monsY1);
    glfwSetWindowSize(wnd, monsX2 - monsX1, monsY2 - monsY1);
    
//    glfwSwapInterval(1);

//    for (int i = 0; i < count; ++i) {
//	if (wnd[i] == nullptr) continue;
//	//const GLFWvidmode* mode = glfwGetVideoMode(monitors[i]);
//	int xpos, ypos;
//	glfwGetMonitorPos(monitors[i], &xpos, &ypos);
//	//glfwSetWindowMonitor();
//	glfwSetWindowPos(wnd[i], xpos, ypos);
//    }

  if (wnd != nullptr) {

    ::glfwMakeContextCurrent(wnd);

    auto start = std::chrono::high_resolution_clock::now();
    
    for (int j = 0; j < 500; ++j) {           
      
//	for (int i = 0; i < count; ++i) {
//	    if (wnd[i] == nullptr) continue;
	    
	    if ((j % 100) == 0) {
		int xpos, ypos, w, h;
		glfwGetWindowPos(wnd, &xpos, &ypos);
		glfwGetWindowSize(wnd, &w, &h);
		std::cout << rank << "\t wnd at (" << xpos << ", " << ypos << ") x (" << w << ", " << h << ")" << std::endl;
	    }
	    

	    ::glClearColor(
		static_cast<float>(::rand()) / static_cast<float>(RAND_MAX),
		static_cast<float>(::rand()) / static_cast<float>(RAND_MAX),
		static_cast<float>(::rand()) / static_cast<float>(RAND_MAX),
		0.0f);

	    ::glClear(GL_COLOR_BUFFER_BIT);
	    
	    ::std::this_thread::sleep_for(std::chrono::milliseconds(10));
    
//	}

	::glfwSwapBuffers(wnd);
	::glfwPollEvents();
    }

    auto end = std::chrono::high_resolution_clock::now();
    double sec = static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()) / 1000.0;
    double fps = 500.0 / sec;
    
    std::cout << rank << "\t" << fps << " fps" << std::endl;
    
    ::glfwDestroyWindow(wnd);
    wnd = nullptr;
  }
  
    ::glfwTerminate();
    MPI_Finalize();   
    return 0;
}
*/