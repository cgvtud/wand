#ifndef WINDOW_H
#define WINDOW_H
#pragma once

#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "box.h"
#include "ViewInteractor.hpp"

class window {
public:
  static bool init();
  static void deinit();
  
  window();
  ~window();
  
  void close();
  inline bool isValid() const {
    return wnd != nullptr;
  }
  
  void doEvents();
  void render();
  
  void resetbbox(const box& nbbox);
  inline glm::mat4 modelView() const {
    return const_cast<ViewInteractor&>(view).getTransformation() * model;
  }
  inline float fovyRad() const {
    return glm::radians(fovyDeg);
  }
  inline float nearClip() const {
    return 0.1f;
  }
  inline float farClip() const {
    return 100.0f;
  }
  
private:
  static window* getWindow(GLFWwindow* wnd);
  static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
  static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
  static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
  static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
  
  void initViewProjMat();
  void updateProjMat();
  
  GLFWwindow *wnd;
  int width, height;
  
  glm::mat4 model, proj;
  float fovyDeg;
  ViewInteractor view;
  bool mouseBtns[3];
  bool modKeys[6];
  
  box bbox;
};

#endif // WINDOW_H
