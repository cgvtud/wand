#ifndef AUTOINIT_H
#define AUTOINIT_H
#pragma once

template<class T>
class autoInit {
public:
  autoInit();
  ~autoInit();
  inline void forceDeinit() {
    if (valid) {
      T::deinit();
      valid = false;
    }
  }
  inline bool isValid() const { return valid; }
  inline operator bool() const { return valid; }
private:
  bool valid;
};

template<class T>
autoInit<T>::autoInit() : valid(false) {
  valid = T::init();
}

template<class T>
autoInit<T>::~autoInit() {
  if (valid) T::deinit();
}

#endif // AUTOINIT_H
