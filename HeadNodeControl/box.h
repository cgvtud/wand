#ifndef BOX_H
#define BOX_H
#include <cmath>

class box {
public:
  typedef float scalar;
  
  box();
  box(scalar x1, scalar y1, scalar z1, scalar x2, scalar y2, scalar z2);
  box(const box& other);
  ~box();
  box& operator=(const box& other);

  inline scalar xc() const { return (x1 + x2) / 2; }
  inline scalar yc() const { return (y1 + y2) / 2; }
  inline scalar zc() const { return (z1 + z2) / 2; }
  inline scalar diag() const {
    return std::sqrt(
        (x2 - x1) * (x2 - x1)
      + (y2 - y1) * (y2 - y1)
      + (z2 - z1) * (z2 - z1));
  }
  inline scalar xma() const { return (x1 > x2) ? x1 : x2; }
  inline scalar yma() const { return (y1 > y2) ? y1 : y2; }
  inline scalar zma() const { return (z1 > z2) ? z1 : z2; }
  
  scalar x1, y1, z1, x2, y2, z2;
  
};

#endif // BOX_H
