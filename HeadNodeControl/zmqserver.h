#ifndef ZMQSERVER_H
#define ZMQSERVER_H

#include <zmq.hpp>
#include <thread>
#include <functional>

class zmqserver {
public:
//  static bool init();
//  static void deinit();
  
  zmqserver(int port = 23452);
  ~zmqserver();
  
  std::function<void(const float*)> setBBoxCallback;
  std::function<const float*()> getModelViewMatrix;
  std::function<float()> getFovyRad;
  std::function<float()> getNearClip;
  std::function<float()> getFarClip;
  
private:
  static void doWorkCallback(zmqserver* obj);
  void doWork();
  
  zmq::context_t context;
  std::thread worker;
  bool run;
  int port;
};

#endif // ZMQSERVER_H
