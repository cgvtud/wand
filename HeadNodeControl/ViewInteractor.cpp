#include "ViewInteractor.hpp"

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifndef M_PI
#define M_PI 3.14159265345
#endif

ViewInteractor::ViewInteractor()
{
	shift = alt = ctrl = false;
	left = right = middle = false;
	wheel_active = false;
	wheel_dir_up = false;
	position_valid = false;
	pos_x = pos_y = 0;
	space = false;


	resetView();

    sensitivity = 2.0f;
}



void ViewInteractor::updateModifiers(bool shift, bool alt, bool ctrl) 
{
	this->shift = shift;
	this->alt = alt;
	this->ctrl = ctrl;
}


void ViewInteractor::updateShift(bool pressed)
{
    this->shift = pressed;
}


void ViewInteractor::updateAlt(bool pressed)
{
    this->alt = pressed;
}


void ViewInteractor::updateCtrl(bool pressed)
{
    this->ctrl = pressed;
}



void ViewInteractor::updateButtons(bool left, bool right, bool middle)
{
	if (this->left == left && this->right == right && this->middle == middle)
		return;

	this->left = left;
	this->right = right;
	this->middle = middle;

	if (alt && left && !right && !middle) 
		reassignFocusFromScene();

	position_valid = false;
}


void ViewInteractor::updateKeys(bool space)
{
	if (space)
		resetView();
	this->space = space;
}


void ViewInteractor::updateLeftButton(bool left)
{
	if (this->left == left)
		return;

	this->left = left;
	position_valid = false;

	if (alt && left && !right && !middle)
		reassignFocusFromScene();
}


void ViewInteractor::updateRightButton(bool right) 
{
	if (this->right == right)
		return;

	this->right = right;
	position_valid = false;
}


void ViewInteractor::updateMiddleButton(bool middle)
{
	if (this->middle == middle)
		return;

	this->middle = middle;
	position_valid = false;
}



void ViewInteractor::updatePosition(float pos_x, float pos_y)
{
	if (position_valid) {
		old_pos_x = this->pos_x;
		old_pos_y = this->pos_y;
	} else {
		old_pos_x = pos_x;
		old_pos_y = pos_y;
		position_valid = true;
	}
	this->pos_x = pos_x;
	this->pos_y = pos_y;

	delta_x = pos_x - old_pos_x;
	delta_y = pos_y - old_pos_y;

	updateTransformation();
}


void ViewInteractor::updateWheel(bool dir_up)
{
	wheel_active = true;
	wheel_dir_up = dir_up;

	updateTransformation();

	wheel_active = false;
}




void ViewInteractor::resetView()
{
	focus = glm::vec3(0, 0, 0);
	up_dir = glm::vec3(0, 1, 0);
	eye = glm::vec3(0, 0, 2);
}



// parts are taken from stereo interactor plugin for the cgv-framework
void ViewInteractor::updateTransformation()
{
	glm::vec3 x, y, z;

	// if the wheel was triggered then move towards the center
	if (wheel_active) {
		if (wheel_dir_up)
			moveTowardsCenter(eye, focus, 0.5);
		else
			moveTowardsCenter(eye, focus, -0.5);
	}
	

	// check for a rotation action
	if (left) {
		// if no modifiers are active then this is a spherical rotation
		if (!shift && !alt && !ctrl) 
			rotateSpherical(eye, focus, up_dir);

		// if shift is pressed then this is a roll
		if (shift && !alt && !ctrl)
			rotateUpDir(up_dir, eye - focus);

		// if ctrl is pressed then move the focus point rather than
		// the eye point
		if (ctrl && !shift && !alt)
			rotateSpherical(focus, eye, up_dir, true);
	}


	// check for a move action which is triggered by the right mouse button
	if (right) {
		// if no modifiers are active then this is just a motion across the
		// the plane orthogonal to the view direction
		if (!shift && !alt && !ctrl)
			moveImagePlane(eye, focus, up_dir);
	}

	// if the middle button was pressed then perform a zoom towards the focus
	if (middle) {
        float dist = delta_x + delta_y;
        moveTowardsCenter(eye, focus, -dist);
	}
}




void ViewInteractor::reassignFocusFromScene()
{
/*	// get the modelview matrix and projection matrix
	double modelview[16], projection[16];
	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, (GLint*)viewport);	
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetDoublev(GL_PROJECTION_MATRIX, projection);	

	// get the Z-value from the depth-buffer
	float pos_z;
	glReadPixels(pos_x, viewport[3]-pos_y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &pos_z);

	// check whether a valid point was hit
	if (glGetError() != GL_NO_ERROR || pos_z<0 || pos_z>1)
		return;

	// convert the point back to openGL object coordinates
	double px, py, pz;
	gluUnProject(pos_x, viewport[3]-pos_y, pos_z, modelview, projection, (GLint*)viewport, &px, &py, &pz);

	// do it in a nice transition
	startTransition();
	focus = Point3D(px, py, pz); */
}




void ViewInteractor::moveImagePlane(glm::vec3 &target, glm::vec3 &center, const glm::vec3 &up)
{
	// create an orthogonal system where Z is the view direction
	// and x and y two orthogonal directions that define the
	// two rotation planes
	glm::vec3 z = glm::normalize(center-target);
	glm::vec3 x = glm::normalize(glm::cross(up,z));
	glm::vec3 y = glm::cross(z,x);

	/*
	if (x.x<0 || y.y<0) {
		x = -1.0*x;
		y = -1.0*y;
	} */

	// FIXME: improve this by using the nearest point at the mouse as reference point



	//double factor = focus.z - eye.z;

	float factor = glm::length(center-target);

	//std::cout<<"Factor: "<<factor<<std::endl;
    float move_x = delta_x*factor;
    float move_y = delta_y*factor;

	target = target + move_x*x + move_y*y;
	center = center + move_x*x + move_y*y;
}





// perform a spherical rotation
// inspired by stereo_view_interactor from Stefan Gumhold
void ViewInteractor::rotateSpherical(glm::vec3 &target, const glm::vec3 &center, glm::vec3 &up, bool swap_y)
{
	if (delta_x == 0 && delta_y == 0)
		return;


    float delta_phi = delta_x*static_cast<float>(M_PI)*sensitivity;
	float delta_theta = delta_y*static_cast<float>(M_PI)*sensitivity;

	if (swap_y)
		delta_theta*=-1.0;

	// create an orthogonal system where Z is the view direction
	// and x and y two orthogonal directions that define the
	// two rotation planes
	glm::vec3 z = glm::normalize(center-target);
	glm::vec3 x = glm::normalize(glm::cross(up,z));
	glm::vec3 y = glm::cross(z,x);

	glm::vec3 axis = delta_theta*x-delta_phi*y;
	float angle = glm::length(axis);
	axis = glm::normalize(axis);
	target = glm::rotate(target-center, angle, axis)+center;
	up = glm::rotate(up_dir, angle, axis);
}



void ViewInteractor::rotateUpDir(glm::vec3 &target, const glm::vec3 &axis)
{

	// calculate the difference angle of the actual position
	// with the center of the window as rotation center and
	// the old position
    float relative_x = pos_x - 0.5f;
    float relative_y = pos_y - 0.5f;
	float angle = atan2(relative_y, relative_x) - atan2(relative_y - delta_y, relative_x - delta_x);

	target = glm::rotate(target, angle, glm::normalize(axis));
}




void ViewInteractor::moveTowardsCenter(glm::vec3& target, glm::vec3& center, float step)
{
	glm::vec3 dir = center-target;

	if (glm::length(dir)<2*step)
		center+=step*glm::normalize(dir);

	target = target + step*glm::normalize(dir);
}




glm::mat4 ViewInteractor::getTransformation() 
{
		return glm::lookAt(eye, focus, up_dir);
}
