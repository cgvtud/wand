#include <iostream>
#include "autoInit.h"
#include "window.h"
#include "zmqserver.h"
#include <glm/gtc/type_ptr.hpp>

int main(int argc, char **argv) {
  autoInit<window> initWindow;
  window mainWnd;
  zmqserver server;
  
  server.setBBoxCallback = [&](const float* bbox) { mainWnd.resetbbox(box(bbox[0], bbox[1], bbox[2], bbox[3], bbox[4], bbox[5])); };
  server.getModelViewMatrix = [&]() { return glm::value_ptr(mainWnd.modelView()); };
  server.getFovyRad = [&]() { return mainWnd.fovyRad(); };
  server.getNearClip = [&]() { return mainWnd.nearClip(); };
  server.getFarClip = [&]() { return mainWnd.farClip(); };
  
  mainWnd.resetbbox(box(-10, 0, 2, -5, 1, 3));
  
  while (mainWnd.isValid()) {
    mainWnd.doEvents();
    mainWnd.render();
  }
  
  return 0;
}
