#include "zmqserver.h"
#include <iostream>
#include <cassert>

//bool zmqserver::init() {
//  return true;
//}

//void zmqserver::deinit() {
//}

zmqserver::zmqserver(int port) : context(1), worker(), run(true), port(port) {
  worker = std::thread(&zmqserver::doWorkCallback, this);

}

zmqserver::~zmqserver() {
  run = false;
  worker.join();
}

void zmqserver::doWorkCallback(zmqserver* obj) {
  obj->doWork();
}

void zmqserver::doWork() {
  zmq::socket_t socket (context, ZMQ_REP);
  socket.bind(std::string("tcp://*:") + std::to_string(port));
  
  socket.setsockopt(ZMQ_SNDTIMEO, 100); // 100ms Timeout
  socket.setsockopt(ZMQ_RCVTIMEO, 100);
  
  while (run) {
    zmq::message_t request;

    if (!socket.recv(&request)) continue;
    if (request.size() < 1) continue; // broken ...
    
    unsigned char code = request.data<const unsigned char>()[0];
    switch (code) {
      case 1: { // set bbox
	assert(request.size() >= 6 * 4 + 1);
	const float *nbb = reinterpret_cast<const float*>(static_cast<const unsigned char*>(request.data()) + 1);
	if (setBBoxCallback) setBBoxCallback(nbb);

	zmq::message_t reply(1);
	static_cast<unsigned char*>(reply.data())[0] = 0; // ok
	socket.send(reply);
	
      } break;
      case 2: { // ask MVP settings
	assert(request.size() >= 1);
	zmq::message_t reply(1 + 4 * 4 * 4 + 3 * 4);
	static_cast<unsigned char*>(reply.data())[0] = 2; // MVP-Data
	float *mvp = reinterpret_cast<float*>(static_cast<unsigned char*>(reply.data()) + 1);
	const float *mvMat = getModelViewMatrix();
	mvp[0] = mvMat[0];
	mvp[1] = mvMat[1];
	mvp[2] = mvMat[2];
	mvp[3] = mvMat[3];
	mvp[4] = mvMat[4];
	mvp[5] = mvMat[5];
	mvp[6] = mvMat[6];
	mvp[7] = mvMat[7];
	mvp[8] = mvMat[8];
	mvp[9] = mvMat[9];
	mvp[10] = mvMat[10];
	mvp[11] = mvMat[11];
	mvp[12] = mvMat[12];
	mvp[13] = mvMat[13];
	mvp[14] = mvMat[14];
	mvp[15] = mvMat[15];
	mvp[16] = getFovyRad();
	mvp[17] = getNearClip();
	mvp[18] = getFarClip();
	socket.send(reply);
	
      } break;
      case 3: { // simple echo
	assert(request.size() >= 2);
	zmq::message_t reply(2);
	reply.data<unsigned char>()[0] = 3;
	reply.data<unsigned char>()[1] = request.data<unsigned char>()[1];
	socket.send(reply);
	
      } break;
      default: {
	zmq::message_t reply(1);
	reply.data<unsigned char>()[0] = 0; // whatever
	socket.send(reply);
      } break;
    }
  }
  
}
