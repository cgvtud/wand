#include "window.h"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

bool window::init() {
  if (::glfwInit() != GL_TRUE) {
    std::cerr << "glfwInit failed" << std::endl;
    return false;
  }
  std::cout << "glfwInit" << std::endl;
  return true;
}

void window::deinit() {
  ::glfwTerminate();
  std::cout << "glfwTerminate" << std::endl;
}

window::window() : wnd(nullptr), width(0), height(0),
    model(), proj(), fovyDeg(60.0f), view(), bbox() {
  mouseBtns[0] = false;
  mouseBtns[1] = false;
  mouseBtns[2] = false;
  modKeys[0] = false;
  modKeys[1] = false;
  modKeys[2] = false;
  modKeys[3] = false;
  modKeys[4] = false;
  modKeys[5] = false;
      
  const float defWidth = 16 * 3 * 50;
  const float defHeight = 9 * 2 * 50;
  width = defWidth;
  height = defHeight;
  
  const GLFWvidmode *vm = glfwGetVideoMode(glfwGetPrimaryMonitor());
  if (vm != nullptr) {
    int pmWidth = vm->width;
    int pmHeight = vm->height;
    
    int width1 = pmWidth * 4 / 5;
    int height1 = static_cast<int>(static_cast<float>(width1) * 9.0f * 2.0f / (16.0f * 3.0f));
    
    int height2 = pmHeight * 4 / 5;
    int width2 =  static_cast<int>(static_cast<float>(width1) * 16.0f * 3.0f / (9.0f * 2.0f));
    
    if (width1 * height1 > width2 * height2) {
      // select the smaller initial window size
      width = width2;
      height = height2;
    } else {
      width = width1;
      height = height1;
    }
  }
  
  //::glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // ogl 3.0 compat
  //::glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  // ::glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); allow old stuff
  //::glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
  
  wnd = ::glfwCreateWindow(width, height, "CGV DisplayWall HeadNode Control", nullptr, nullptr);
  
  if (wnd == nullptr) {
    std::cout << "glfwCreateWindow failed" << std::endl;
    return;
  }
  
  ::glfwSetWindowUserPointer(wnd, this);
  ::glfwSetKeyCallback(wnd, &window::key_callback);
  ::glfwSetCursorPosCallback(wnd, &window::cursor_position_callback);
  ::glfwSetMouseButtonCallback(wnd, &window::mouse_button_callback);
  ::glfwSetScrollCallback(wnd, &window::scroll_callback);

  ::glfwMakeContextCurrent(wnd);
  ::glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  
  initViewProjMat();
  
}

void window::key_callback(GLFWwindow* wnd, int key, int scancode, int action, int mods) {
  window *that = window::getWindow(wnd);
  
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    ::glfwSetWindowShouldClose(wnd, GL_TRUE);
  }
  if (key == GLFW_KEY_HOME && action == GLFW_PRESS) {
    that->resetbbox(that->bbox);
  }
  
  if (key == GLFW_KEY_LEFT_SHIFT) that->modKeys[0] = action != GLFW_RELEASE;
  if (key == GLFW_KEY_LEFT_CONTROL) that->modKeys[1] = action != GLFW_RELEASE;
  if (key == GLFW_KEY_LEFT_ALT) that->modKeys[2] = action != GLFW_RELEASE;
  if (key == GLFW_KEY_RIGHT_SHIFT) that->modKeys[3] = action != GLFW_RELEASE;
  if (key == GLFW_KEY_RIGHT_CONTROL) that->modKeys[4] = action != GLFW_RELEASE;
  if (key == GLFW_KEY_RIGHT_ALT) that->modKeys[5] = action != GLFW_RELEASE;
  that->view.updateModifiers(
    that->modKeys[0] || that->modKeys[3],
    that->modKeys[1] || that->modKeys[4],
    that->modKeys[2] || that->modKeys[5]);
 
}

void window::cursor_position_callback(GLFWwindow* wnd, double xpos, double ypos) {
  window *that = window::getWindow(wnd);
  that->view.updatePosition(
    static_cast<float>(xpos) / static_cast<float>(that->width),
    static_cast<float>(ypos) / static_cast<float>(that->height));  
}

void window::mouse_button_callback(GLFWwindow* wnd, int button, int action, int mods) {
  window *that = window::getWindow(wnd);
  switch (button) {
    case GLFW_MOUSE_BUTTON_LEFT: that->mouseBtns[0] = action == GLFW_PRESS; break;
    case GLFW_MOUSE_BUTTON_RIGHT: that->mouseBtns[1] = action == GLFW_PRESS; break;
    case GLFW_MOUSE_BUTTON_MIDDLE: that->mouseBtns[2] = action == GLFW_PRESS; break;
  }
  that->view.updateButtons(that->mouseBtns[0], that->mouseBtns[1], that->mouseBtns[2]);
}

void window::scroll_callback(GLFWwindow* wnd, double xoffset, double yoffset) {
  window *that = window::getWindow(wnd);
  if (yoffset < 0) that->view.updateWheel(true);
  if (yoffset > 0) that->view.updateWheel(false);
}

window::~window() {
  close();
}

void window::close() {
  if (wnd != nullptr) {
    ::glfwDestroyWindow(wnd);
    wnd = nullptr;
  }
}

void window::doEvents() {
  if (wnd == nullptr) return;
  ::glfwPollEvents();
  
  int w, h;
  ::glfwGetFramebufferSize(wnd, &w, &h);
  if ((width != w) || (height != h)) {
    width = w;
    height = h;
    ::glfwMakeContextCurrent(wnd);
    glViewport(0, 0, w, h);
    updateProjMat();    
  }
  
  if (::glfwWindowShouldClose(wnd)) {
    close();
  }
}

void window::render() {
  if (wnd == nullptr) return;
  ::glfwMakeContextCurrent(wnd);
  ::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  ::glMatrixMode(GL_PROJECTION);
  ::glLoadMatrixf(glm::value_ptr(proj));
  
  ::glMatrixMode(GL_MODELVIEW);
  ::glLoadMatrixf(glm::value_ptr(modelView()));
  
  ::glBegin(GL_LINES);

  ::glColor3ub(192, 192, 192);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z2);

  ::glVertex3f(bbox.x1, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z2);

  ::glVertex3f(bbox.x1, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y1, bbox.z2);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x1, bbox.y2, bbox.z2);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z1);
  ::glVertex3f(bbox.x2, bbox.y2, bbox.z2);
  
  ::glColor3ub(255, 0, 0);
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zc());
  ::glVertex3f(bbox.xma(), bbox.yc(), bbox.zc());
  
  ::glColor3ub(0, 255, 0);
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zc());
  ::glVertex3f(bbox.xc(), bbox.yma(), bbox.zc());
  
  ::glColor3ub(32, 64, 255);
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zc());
  ::glVertex3f(bbox.xc(), bbox.yc(), bbox.zma());
  
  ::glEnd();
  
  ::glfwSwapBuffers(wnd);
}

void window::resetbbox(const box& nbbox) {
  bbox = nbbox;
  
  view.updateKeys(true); // reset the view matrix
  view.updateKeys(false);
  
  initViewProjMat(); 
}

window* window::getWindow(GLFWwindow* wnd) {
  return static_cast<window*>(glfwGetWindowUserPointer(wnd));
}

void window::initViewProjMat() {
  model = 
    glm::translate(
    glm::scale(
      glm::mat4(1.0f),
      glm::vec3(2.0f / bbox.diag())),
      glm::vec3(-bbox.xc(), -bbox.yc(), -bbox.zc()));
  updateProjMat();
}

void window::updateProjMat() {
  proj = glm::perspective(fovyRad(),
    static_cast<float>(width) / static_cast<float>(height),
    nearClip(), farClip()); // TODO: near and far should be computed from the bbox
}
